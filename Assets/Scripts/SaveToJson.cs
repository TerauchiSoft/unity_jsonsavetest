﻿using System.IO;
using UnityEngine;

/// <summary>
/// セーブデータ全ての初期化、読み出し、セーブ。
/// </summary>
public class SaveToJson : MonoBehaviour {
    private const string PATH_SAVE_DATA_DIRECTORY = "/save";
    private int numSaveFile;

    private SaveData saveData;
    private ISavingText<SaveData>[] savingTexts = new ISavingText<SaveData>[10];

	// Use this for initialization
	void Start () {
        InitSaveData();
    }
	
    void InitSaveData()
    {
        string dir = Application.streamingAssetsPath + PATH_SAVE_DATA_DIRECTORY;
        if (!File.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        numSaveFile = Directory.GetFiles(dir, "*", SearchOption.AllDirectories).Length;

        saveData = new SaveData();

        for (int i = 0; i < numSaveFile; i++)
        {
            savingTexts[i] = new SavingData<SaveData>();
            savingTexts[i].InitSetting(i);
        }
    }

    /// <summary>
    /// 全てのデータのセーブ。セーブデータがなければ新しく作成する。
    /// </summary>
    [ContextMenu("Save")]
    public void SaveToData()
    {
        BehaviourData beh = BehaviourData.Instance;
        saveData.intdata = beh.intdata;
        saveData.stringdata = beh.stringdata;
        if (savingTexts == null)
        {
            savingTexts = new ISavingText<SaveData>[1];
        }

        if (savingTexts[0] == null)
        {
            savingTexts[0] = new SavingData<SaveData>();
            savingTexts[0].InitSetting(0);
        }
        savingTexts[0].Save(saveData);
    }

    /// <summary>
    /// 全てのデータのロード。ロードデータがなければ初期セーブデータを読み込む。
    /// </summary>
    [ContextMenu("Load")]
    public void LoadData()
    {
        saveData = savingTexts[0].Load();
        BehaviourData.Instance.intdata = saveData.intdata;
        BehaviourData.Instance.stringdata = saveData.stringdata;
    }
}
