﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BehaviourData : Singleton<BehaviourData>, ILoadableObject<BehaviourData>
{
    public int intdata = 4;
    public string stringdata = "string";

    public void Load(BehaviourData beh)
    {
        this.intdata = beh.intdata;
        this.stringdata = beh.stringdata;
    }
}
