﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISavingText<T>{
    /// <summary>
    /// データのインデックスに沿ってパスを取得。
    /// </summary>
    /// <param name="idx">Index.</param>
    void InitSetting(int idx);

    /// <summary>
    /// データをJson化してセーブする。
    /// </summary>
    /// <param name="saveData">Save data.</param>
    void Save(T saveData);

    /// <summary>
    /// Jsonからクラスにして渡す。
    /// </summary>
    /// <returns>The load.</returns>
    T Load();
}
