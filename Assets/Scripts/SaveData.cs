﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveData : ISerializationCallbackReceiver
{
    public int intdata = 4;
    public string stringdata = "string";

    public SaveData()
    {

    }

    public void OnBeforeSerialize()
    {
        Debug.Log("Serialize前にごにょごにょいじれる");
    }

    public void OnAfterDeserialize()
    {
        Debug.Log("Deserialize後にごにょごにょいじれる");
    }
}
